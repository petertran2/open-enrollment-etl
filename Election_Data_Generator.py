"""
Generates a file of hypothetical data containing employee's
and their dependents' Open Enrollment (OE) elections.

"""

from random import randrange, choice

""" Declaring and assigning global variables """
ssnList = [] # SSNs placed into here cannot be re-generated
rel = 'E'
count = 0
selection = {'Medical': 'Waive', 'Dental': 'Waive', \
             'Vision': 'Waive'}
tier = {'Medical': 'Waive', 'Dental': 'Waive', 'Vision': 'Waive'}
EE = {'Medical': False, 'Dental': False, 'Vision': False}
SP = {'Medical': False, 'Dental': False, 'Vision': False}
CH = {'Medical': False, 'Dental': False, 'Vision': False}

def generateEmpSSN(minSSN, maxSSN):
    """ Generates employee SSN between minSSN and maxSSN """
    global ssnList, emp
    while rel == 'E':
        emp = randrange(minSSN, maxSSN)
        if emp not in ssnList:
            ssnList.append(emp)
            break

def generateDepSSN(minSSN, maxSSN):
    """ Generates dependent SSN between minSSN and maxSSN """
    global ssnList, dep
    while rel != 'E':
        dep = randrange(minSSN, maxSSN)
        if dep not in ssnList:
            ssnList.append(dep)
            break

def empElect():
    """ Generates employee elections based on available plans """
    global rel, selection, EE
    medicalPlan = ('Gold', 'Silver', 'Bronze', 'Waive')
    dentalPlan = ('Basic', 'Enhanced', 'Waive')
    visionPlan = ('Coverage', 'Waive')
    if rel == 'E':
        selection['Medical'] = choice(medicalPlan)
        selection['Dental'] = choice(dentalPlan)
        selection['Vision'] = choice(visionPlan)
        for i in selection:
            if selection[i] != 'Waive':
                EE[i] = True
            elif selection[i] == 'Waive':
                EE[i] = False

def depElect(noMedChance, noDenChance, noVisChance, allWaive):
    """ Allows for chances of dependents being waived from
        one or more benefits. Otherwise, elect the same
        plans as their corresponding employee """
    global rel, selection, SP, CH
    if rel != 'E':
        if randrange(noMedChance) == 0:
            selection['Medical'] = 'Waive'
        if randrange(noDenChance) == 0:
            selection['Dental'] = 'Waive'
        if randrange(noVisChance) == 0:
            selection['Vision'] = 'Waive'
        if randrange(allWaive) == 0:
            selection['Medical'] = 'Waive'
            selection['Dental'] = 'Waive'
            selection['Vision'] = 'Waive'
        if rel == 'S':
            for i in selection:
                if selection[i] != 'Waive':
                    SP[i] = True
                elif selection[i] == 'Waive':
                    SP[i] = False
        elif rel == 'C':
            for i in selection:
                if selection[i] != 'Waive' or CH[i] == True:
                    CH[i] = True
                elif selection[i] == 'Waive' and CH[i] == False:
                    CH[i] = False

def coverage():
    """ Writes out a person's benefit elections """
    global emp, dep, rel, selection
    return str(emp) + '|' + rel + '|' + \
           (str(dep) if rel != 'E' else str(emp)) \
           + '|Medical|' + selection['Medical'] \
           + '|Dental|' + selection['Dental'] \
           + '|Vision|' + selection['Vision'] + '\n'

def nextRel(spChance, firstChChance, nextChChance):
    """ Determines whether next row is employee, 
        spouse, or child based on user-determined chances"""
    global rel, selection
    if rel == 'E':
        """ If the current row is an employee, then the next
            row will either be a spouse, child, or employee """
        if randrange(spChance) == 1:
            rel = 'S'
        elif randrange(firstChChance) == 1:
            rel = 'C'
    else:
        """ If the current row is a dependent, then the next
            row will either be a child or employee. This is
            because an employee may have only one spouse """
        if randrange(nextChChance) == 1:
            rel = 'C'
        else:
            rel = 'E'

def tiers():
    """ Returns the employee's tiers for each benefit """
    global EE, SP, CH, tier
    tierLine = ''
    if rel == 'E':
        for i in EE:
            if EE[i] == True and SP[i] == False and CH[i] == False:
                tier[i] = 'EEOnly'
            elif EE[i] == True and SP[i] == True and CH[i] == False:
                tier[i] = 'EESpouse'
            elif EE[i] == True and SP[i] == False and CH[i] == True:
                tier[i] = 'EEChild'
            elif EE[i] == True and SP[i] == True and CH[i] == True:
                tier[i] = 'EEFamily'
        tierLine = 'Tier|' + tier['Medical'] + \
                   '|' + tier['Dental'] + \
                   '|' + tier['Vision'] + '\n'
        # Resets all tier data for the next employee
        EE = {'Medical': False, 'Dental': False, 'Vision': False}
        SP = {'Medical': False, 'Dental': False, 'Vision': False}
        CH = {'Medical': False, 'Dental': False, 'Vision': False}
        tier = {'Medical': 'Waive', 'Dental': 'Waive', 'Vision': 'Waive'}
    return tierLine

def generateData(rows = 1000):
    """ Generates the file with a specified number of rows """
    global count
    with open('Election_Data.txt', 'w') as f:
        while count < rows:    # Number of rows in the file
            generateEmpSSN(100000000, 999999999)
            generateDepSSN(100000000, 999999999)
            empElect()
            depElect(9, 8, 7, 6)
            f.write(coverage())
            nextRel(2, 2, 2)
            f.write(tiers())
            count += 1
