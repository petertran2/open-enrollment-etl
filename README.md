# Open Enrollment ETL

![Excel Dashboard](https://gitlab.com/petertran2/open-enrollment-etl/-/raw/master/README_assets/oe_etl_demo.JPG)

This project generates a text file of hypothetical data containing employee's and their dependents' Open Enrollment (OE) elections. It will then extract, transform, and load the contents of the text file into an SQL table. It will then transform and load the contents of the SQL database into an Excel file. Finally, an Excel Dashboard will use this data to perform data visualization.

## Technologies

- Python
- Pandas
- Python ODBC
- OpenPyXL
- Microsoft SQL Server
- Microsoft Excel

### Author

Peter Tran