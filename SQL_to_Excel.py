"""
Transforms and loads SQL data into Excel.

"""

import pyodbc, pandas, openpyxl

def SQLtoExcel():
    """ Transforms and loads SQL data into Excel """
    
    sql = pyodbc.connect(""" DRIVER={SQL Server};
    SERVER=FUSU-PC\SQLEXPRESS;DATABASE=OPEN_ENROLLMENT;
    Trusted_Connection=yes' """)

    query = """ select ROW_SSN 'Participant SSN', RELATION
    'Relation', EMPLOYEE_SSN 'Employee SSN', PLAN_1 'Medical Plan',
    TIER_1 'Medical Tier', EMPLOYEE_COST_1 'Medical EE Cost',
    EMPLOYER_COST_1 'Medical ER Cost', PREMIUM_COST_1
    'Medical Premium Cost', PLAN_2 'Dental Plan', TIER_2
    'Dental Tier', EMPLOYEE_COST_2 'Dental EE Cost',
    EMPLOYER_COST_2 'Dental ER Cost', PREMIUM_COST_2
    'Dental Premium Cost', PLAN_3 'Vision Plan', TIER_3
    'Vision Tier', EMPLOYEE_COST_3 'Vision EE Cost',
    EMPLOYER_COST_3 'Vision ER Cost', PREMIUM_COST_3
    'Vision Premium Cost' from elections """
    
    data = pandas.read_sql_query(query, sql)
    writer = pandas.ExcelWriter('Open Enrollment Elections.xlsx')
    data.to_excel(writer, sheet_name='Data')
    writer.save()

def cleanExcel():
    """ Cleans the Excel data """
    wb = openpyxl.load_workbook('Open Enrollment Elections.xlsx')
    sheet = wb['Data']
    sheet.delete_cols(1)
    wb.save('Open Enrollment Elections.xlsx')

def loadExcel():
    """ Transforms and loads SQL data into Excel """
    SQLtoExcel()
    cleanExcel()
