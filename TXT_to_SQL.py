"""
Loads Open Enrollment election data from a text
file and rates from a csv file into a SQL database.

"""

import pyodbc, csv

""" Declare and assign global variables """
sql = pyodbc.connect(""" DRIVER={SQL Server};
SERVER=FUSU-PC\SQLEXPRESS;DATABASE=OPEN_ENROLLMENT;
Trusted_Connection=yes' """)
c = sql.cursor()
matrix=[] # Rates are loaded into here

def clearTable():
    """ Clears SQL table before load """
    global c
    c.execute("delete from elections")

def txtToSQL():
    """ Extracts, transforms, and loads text file data """
    global c
    with open('Election_Data.txt', 'r') as textFile:
        ssn = ''
        for line in textFile:
            data = line.rstrip('\n').split('|')
            if data[0] != 'Tier':
                ssn = data[0]
                query = (""" insert into elections(EMPLOYEE_SSN,
                         RELATION,ROW_SSN,BENEFIT_1,PLAN_1,
                         BENEFIT_2,PLAN_2,BENEFIT_3,PLAN_3)
                          values (?, ?, ?, ?, ?, ?, ?, ?, ?) """)
                c.execute(query, *data)
            else:
                if data[1] != 'Waive':
                    c.execute(""" update elections set TIER_1 = ?
                               where EMPLOYEE_SSN = ?
                               and PLAN_1 <> 'Waive' """,
                              data[1],ssn)
                    c.execute(""" update elections set TIER_1 =
                               'Waive' where EMPLOYEE_SSN = ?
                               and PLAN_1 = 'Waive' """,ssn)
                else:
                    c.execute(""" update elections set TIER_1 = ?
                               where EMPLOYEE_SSN = ? """,
                              data[1],ssn)
                if data[2] != 'Waive':
                    c.execute(""" update elections set TIER_2 = ?
                               where EMPLOYEE_SSN = ?
                               and PLAN_2 <> 'Waive' """,
                              data[2],ssn)
                    c.execute(""" update elections set TIER_2 = 
                              'Waive' where EMPLOYEE_SSN = ?
                               and PLAN_2 = 'Waive' """,ssn)
                else:
                    c.execute(""" update elections set TIER_2 = ?
                               where EMPLOYEE_SSN = ? """,
                              data[2],ssn)
                if data[3] != 'Waive':
                    c.execute(""" update elections set TIER_3 = ?
                               where EMPLOYEE_SSN = ?
                               and PLAN_3 <> 'Waive' """,
                              data[3],ssn)
                    c.execute(""" update elections set TIER_3 =
                               'Waive' where EMPLOYEE_SSN = ?
                               and PLAN_3 = 'Waive' """,ssn)
                else:
                    c.execute(""" update elections set TIER_3 = ?
                               where EMPLOYEE_SSN = ? """,
                              data[3],ssn)

def rateMatrix():
    """ Loads rates into matrix """
    global matrix
    with open('Rates.csv', 'r') as csvFile:
        data = csv.reader(csvFile)
        for row in data:
            matrix.append(row)

def loadRates():
    """ Calculates and loads rates into the SQL table """
    global c, matrix
    for row in matrix:
        c.execute(""" update elections set EMPLOYEE_COST_1 = ?,
        EMPLOYER_COST_1 = ?, PREMIUM_COST_1 = ? where
        BENEFIT_1 = ? and PLAN_1 = ? and TIER_1 = ? """,
                  row[3],row[4],float(row[3])+float(row[4]), \
                  row[0],row[1],row[2])
        c.execute(""" update elections set EMPLOYEE_COST_2 = ?,
        EMPLOYER_COST_2 = ?, PREMIUM_COST_2 = ? where
        BENEFIT_2 = ? and PLAN_2 = ? and TIER_2 = ? """,
                  row[3],row[4],float(row[3])+float(row[4]), \
                  row[0],row[1],row[2])
        c.execute(""" update elections set EMPLOYEE_COST_3 = ?,
        EMPLOYER_COST_3 = ?, PREMIUM_COST_3 = ? where
        BENEFIT_3 = ? and PLAN_3 = ? and TIER_3 = ? """,
                  row[3],row[4],float(row[3])+float(row[4]), \
                  row[0],row[1],row[2])

def cleanData():
    """ Cleans the SQL data """
    global c
    c.execute(""" delete from elections where tier_1 is null """)

def saveData():
    """ Saves SQL data """
    global sql
    sql.commit()

def loadSQL():
    """ Loads text file data and rates into SQL """
    clearTable()
    txtToSQL()
    rateMatrix()
    loadRates()
    cleanData()
    saveData()
