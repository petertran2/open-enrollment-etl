"""
Generates Open Enrollment election data.
Loads text file data into SQL.
Loads SQL data into Excel.

"""

from Election_Data_Generator import *
from TXT_to_SQL import *
from SQL_to_Excel import *

print('Generating data...')
generateData()
print('Loading into SQL...')
loadSQL()
print('Loading into Excel...')
loadExcel()
print('Done!')
